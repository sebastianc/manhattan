<h2>Env Settings</h2>
<p align="center">
DB_CONNECTION=sqlite
</p>
<p align="center">
DB_DATABASE=/absolute/path/to/database.sqlite
</p>
<h2>Sqlite driver</h2>
<p align="center">
Driver that allows you to use PHP with SQLite:
</p>
<p align="center">
sudo apt-get install php7.0-sqlite3
</p>
<p align="center">
For any issues on Linux with the driver install run:
</p>
<p align="center">
sudo apt-get update
</p>
<h2>Migrations</h2>
<p align="center">
Run this once for Sqlite before running migrations:
</p>
<p align="center">
php artisan migrate:install --env=local
</p>
<p align="center">
</p>
<p align="center">
Then always migrate with:
</p>
<p align="center">
php artisan migrate --env=local
</p>