<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['prefix' => 'v1'], function ($request) {

    //Account management
    Route::group(['prefix' => 'auth'], function ($request) {
        Route::post('/login', 'API\V1\AuthController@login');
        Route::post('/register', 'API\V1\AuthController@register');
        Route::get('/logout', 'API\V1\AuthController@logout');
    });

    // Authenticated Routes
    Route::group(['middleware' => 'auth:api'], function ($request) {

        //Api info
        Route::group(['prefix' => 'auth'], function ($request) {
            Route::get('/appdata', 'API\V1\AuthController@appdata');
        });

        //Products
        Route::group(['prefix' => 'products'], function ($request) {
            Route::get('/', 'API\V1\ProductController@viewAll');
            Route::get('/{product_id}', 'API\V1\ProductController@get');
        });

        //Orders
        Route::group(['prefix' => 'orders'], function ($request) {
            Route::get('/', 'API\V1\OrderController@viewAll');
            Route::get('/{order_id}', 'API\V1\OrderController@get');
        });

        //Users
        Route::group(['prefix' => 'users'], function ($request) {
            Route::get('/', 'API\V1\UserController@getUsers');
            Route::get('/orders', 'API\V1\UserController@getOrders');
            Route::get('/my-profile/{id}', 'API\V1\UserController@getById');
        });

    });

});
