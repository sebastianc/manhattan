<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 02/06/2019
 * Time: 14:44
 */

namespace App\Http\Controllers\API\V1;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class AuthController extends ApiController
{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;

    /**
     * Log in a user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        //Limit the amount of times users can login
        if ($this->hasTooManyLoginAttempts($request)) {
            return parent::api_response([], false, 'too many authentication attempts, try again in '.$this->lockoutTime.' seconds', 401);
        }

        $tempUser = User::where('username', $credentials['username'])
            ->where('active', 1)
            ->first();

        $token = Auth::attempt($credentials, ['role' => $tempUser['type']]);

        //If the login attempt failed
        if (!$token) {
            $this->incrementLoginAttempts($request);
            return parent::api_response([], false, 'invalid credentials', 401);
        }

        $user = User::findOrFail(Auth::user()->id);

        return parent::api_response([
            'token' => $token,
            'user' => $user
        ],
            true,
            'User with username '.$user['username'].' is now Logged in');
    }

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = Input::only('username', 'password', 'forename', 'surname', 'type', 'gender', 'mobile_phone', 'street', 'city', 'postcode', 'api_key');

        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users|max:255',
            'password' => 'required|min:8',
            'forename' => 'min:2|max:42',
            'surname' => 'min:2|max:42',
            'type' => 'min:2|max:191',
            'api_key' => 'required'
        ]);
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
        } elseif($data['api_key']=='asj895thA*(%Qashj5h') {
            $new_user = new User;
            $new_user->username = $data['username'];
            $new_user->password = bcrypt($data['password']);
            if(!empty($data['forename'])) {
                $new_user->forename = $data['forename'];
            } else {
                $new_user->forename = '';
            }
            if(!empty($data['surname'])) {
                $new_user->surname = $data['surname'];
            } else {
                $new_user->surname = '';
            }
            $new_user->active = true;
            if(!empty($data['type'])) {
                $new_user->type = $data['type'];
            } else {
                $new_user->type = 'subscriber';
            }

            if($new_user->save()) {
                $request = new Request();
                $request->merge($data);
                return $this->login($request);
            }
        }
        // In case shake hand fails leave it empty for suspected bruteforce
        // Security by obfuscation is a way, also we would need some ip limiting in the .env if it's an internal API
    }

    /**
     * Logout user
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        if(!Auth::logout()){
            $data = Input::only('token');
            $validator = Validator::make($request->all(), [
                'token' => 'required|max:355'
            ]);
            if (!$validator->fails()) {
                if ($data['token']) {
                    Auth::invalidate($data['token']);
                }
            } else {
                return parent::api_response([], false, ['Error' => 'user not found'], 200);
            }
            return $this->api_response([], true, ['Success' => 'logged out'], 200);
        }
    }

    /**
     * Reset password
     * @return mixed
     */
    public function reset()
    {
        $username = stripslashes(Input::get('username'));
        $user = User::where('username', $username)->first();

        if($user){
            $raw_pass = $this->generateRandomString(12);
            $new_pass = bcrypt($raw_pass);
            $user->password = $new_pass;
            $data['user'] = $user->toArray();
            $data['password'] = $raw_pass;
            if($user->parse_id && !$user->initial_pass_reset){
                $user->initial_pass_reset = 1;
            }
            if($user->save()){
                Mail::send('emails.API.V1.reset', ['data' => $data], function ($m) use ($username, $data) {
                    $m->from('hey@c2o.com', 'C2O');

                    $m->to($username, 'user')->subject('Pass Reset');
                });
                return parent::api_response([], true, ['Success' => 'new pass sent'], 200);
            }

        }

        return parent::api_response([], false, ['Error' => 'user not found'], 404);

    }

    /**
     * Generate a string after Laravel's str_random was deprecated
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /* General Information about the app
    * @param Request
    * @return mixed
    */
    public function appData(Request $request)
    {

        $data = Input::only('api_key');

        $validator = Validator::make($request->all(), [
            'api_key' => 'required'
        ]);

        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 404);
        } elseif($data['api_key']=='asj895thA*(%Qashj5h') {

            $orders = Order::orderby('id')->orderby('name')->get();
            $products = Product::orderby('name')->get();

            $data = [
                'orders' => $orders,
                'products' => $products,
            ];

            return parent::api_response($data, true, ['Success' => 'Appdata'], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Access Denied'], 401);
        }
    }

    public function update(Request $request)
    {
        $app = $request->input('app');
        $appVersion = config('c2o.version.'. $app);

        if ($appVersion == null) {
            return parent::api_response([], false, ['error' => 'invalid app parameter'], 200);
        }

        $shouldUpdate = version_compare($appVersion, $request->input('version'), '>');

        $data = [
            'should_update' => $shouldUpdate
        ];

        return parent::api_response($data, true, ['Success' => 'update'], 200);
    }


}