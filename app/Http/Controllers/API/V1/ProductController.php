<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 02/06/2019
 * Time: 15:55
 */

namespace App\Http\Controllers\API\V1;

use App\Models\Product;

class ProductController extends ApiController
{
    function viewAll(){

        $products = Product
            ::orderBy('created_at', 'desc')
            ->get();

        return parent::api_response($products, true, ['return' => 'All Products'], 200);
    }

    function get($id){
        $product = Product::find($id);
        if($product) {
            return parent::api_response($product, true, ['return' => 'Selected Product with id '.$id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

}