<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 02/06/2019
 * Time: 17:02
 */

namespace App\Http\Controllers\API\V1;

use App\Models\Order;

class OrderController extends ApiController
{
    function viewAll(){

        $orders = Order
            ::orderBy('created_at', 'desc')
            ->get();

        return parent::api_response($orders, true, ['return' => 'All Products'], 200);
    }

    function get($id){
        $order = Order::find($id);
        if($order) {
            return parent::api_response($order, true, ['return' => 'Selected Product with id '.$id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

}