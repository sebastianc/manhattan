<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 02/06/2019
 * Time: 15:46
 */


namespace App\Http\Controllers\API\V1;


use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserController extends ApiController
{

    public function getOrders() {

        $orders = Order::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->orderBy('name', 'asc')
            ->paginate(10);

        return parent::api_response($orders, true, ['return' => 'Orders of current user '], 200);
    }

    public function getUsers()
    {
        $users = User::orderBy('is_admin', 'desc')
            ->orderBy('created_at', 'desc')
            ->orderBy('surname', 'asc')
            ->paginate(15);
        return parent::api_response($users, true, ['return' => 'all users']);
    }

    function getById($id)
    {
        $user = User::find($id);
        if ($user) {
            return parent::api_response($user, true, ['return' => 'User with id ' . $id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

}