<?php
/**
 * Created by: Ben Lewis
 * Date: 22/05/2019
 * Time: 09:46
 */

namespace App\Interfaces;


use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface OrderInterface
{
	/**
	 * Return a collection of orders
	 * @param int $limit. Limit total orders returned
	 * @param int $offset. Offset starting point for orders returned
	 * @param array $statuses. Order statuses to constrain return
	 * @return Order[]
	 */
	public function all( int $limit, int $offset, array $statuses );

	/**
	 * Find a specific Order
	 * @param int $id
	 * @return Order
	 * @throws ModelNotFoundException
	 */
	public function findById( int $id );
}