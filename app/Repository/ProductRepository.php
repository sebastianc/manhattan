<?php
/**
 * Created by: Ben Lewis
 * Date: 22/05/2019
 * Time: 09:47
 */

namespace App\Repository;

use App\Interfaces\ProductInterface;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository implements ProductInterface
{
    /**
     * The model associated with the repository.
     *
     * @var Model
     */
    protected $model;

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * Create a new repository instance.
     *
     * @param Product $model
     *
     * @return void
     */
    public function __construct(Product $model)
    {
        $this->setModel($model);
    }

    /**
     * Set the model of the repository.
     *
     * @param Model $model
     *
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Return Order collection from the database.
     * $columns leaves room for a LengthAwarePaginator implementation
     *
     * @param int $limit
     * @param int $offset
     * @param array $statuses
     * @param array $columns
     *
     * @return Collection
     */
    public function all(int $limit = null, int $offset = null, array $statuses = [], array $columns = ['*'])
    {
        return $this->model
            ->whereIn('status', $statuses)
            ->skip($offset)
            ->take($limit)
            ->paginate($this->perPage, $columns);
    }

    /**
     * Return Order collection by id.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findById( int $id )
    {
        if($id) {
            return $this->model
                ->where('id', $id)
                ->get();
        } else {
            return collect();
        }
    }
}