<?php
/**
 * Created by: Ben Lewis
 * Date: 21/05/2019
 * Time: 15:21
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	protected $connection = 'mysql';
}