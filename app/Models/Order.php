<?php
/**
 * Created by: Ben Lewis
 * Date: 21/05/2019
 * Time: 15:23
 */

namespace App\Models;


class Order extends BaseModel
{
	public function products()
	{
		return $this->belongsToMany( Product::class, 'orders_products', 'order_id', 'product_id' )->withPivot( 'qty' );
	}
}