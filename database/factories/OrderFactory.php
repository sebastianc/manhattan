<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'orders_status' =>  $faker->randomElement( [ 1, 2, 3 ] ),
    ];
});
