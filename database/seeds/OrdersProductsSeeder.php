<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Order;
use Faker\Factory as Faker;

class OrdersProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$orders = Order::all();
    	$products = Product::all();
    	$faker = Faker::create();

    	$orders->each( function ( Order $order ) use ( $products, $faker ){
    		$productsCount = $faker->numberBetween( 1, 25 );

			for( $i = 0;  $i < $productsCount; $i++ ){
				$product = $faker->randomElement( $products );
				$productsQty = $faker->numberBetween( 1, 200 );

				$order->products()->save( $product, [ 'qty' => $productsQty ] );
			}
		});
    }
}
